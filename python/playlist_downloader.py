
'''
 Simple snippet to download all songs into a  youtube playlist in mp3 format
 Third-party libraries

 BeautifulSoup:  Leonard Richardson https://www.crummy.com/software/BeautifulSoup/
 pafy: https://github.com/mps-youtube/pafy
 pydub: James Robert https://github.com/jiaaro/pydub
'''


from bs4 import BeautifulSoup 
import urllib.request 
import pafy 
from pydub import AudioSegment
import os
import sys

def getvideosid(url_playlist):

	"""
	 Retrive a id list of the all videos into a playlist
	 url_playlist: a  string than contains the URL playlist
	 return: a id list of type string 
	"""

	req = urllib.request.urlopen(url_playlist)
	soup = BeautifulSoup(req,"lxml")

	my_videos_tr=soup.find_all("tr",{"class":"pl-video yt-uix-tile "})
	id_list = []
	for tr in my_videos_tr:
		id_list.append(tr.attrs['data-video-id'])

	return id_list


def downloadmp3(path,id_list):
	"""
	 Download a playlist in the folder selected
	 path: a string than contains the path where the music will be saved
	 id_list: a list of strings than contains all videos id
	 return : nothing
	"""

	youtube_pattern = "https://www.youtube.com/watch?v="

	for current_id in id_list:
		video = pafy.new(youtube_pattern+current_id)
		audio = video.getbestaudio()

		original_file=audio.download(filepath="/tmp/",quiet=False)
		#save the original file in a temporal folder
		audio_from_file  = AudioSegment.from_file(original_file)
		
		#get the new file name
		new_file_name = video.title+".mp3"
		#convert to mp3
		audio_from_file.export(path+"/"+new_file_name,format="mp3",bitrate="128k")
		#remove the temporal file
		os.remove(original_file)






def main():

	#Get all paremters passed to the program
    params=sys.argv[1:]

    #check if parameters numbers is greater than 2 
    if len(params)<2:
    	print("Choose the path and the playlist url to download, sample: './videofilter <path> <url>'")
    	sys.exit(0)

    # path where the files will be saved
    path = params[0]
    # URL of the playlist
    url = params[1]



    print('Getting all videos list')
    id_list = getvideosid(url)

    print("Downloading")
    downloadmp3(path, id_list)
    print("Finished files saved in {0}".format(path))

if __name__=="__main__":
	main()