#include "zstring.h"

ZString::ZString()
{
    zPtrString=nullptr;
    zLength=0;
}

ZString::ZString(const char* source)
{
    zPtrString=nullptr;
    if(source!=nullptr)
    {
        zCopyAlloc(source);
    }
}

ZString::ZString(const ZString& source)
{
    zPtrString=nullptr;
    zCopyAlloc(source.to_Cstr());
}

void ZString::zCopy(const char* source,char* destination)
{
    int i=0;
    for(i=0;source[i];i++)
    {
        destination[i]=source[i];
    }
    destination[i]='\0';
}

void ZString::zCopyAlloc(const char* source)
{
    if(zPtrString!=nullptr)
        delete[] zPtrString;

    zUlong len=zStrLen(source);
    zPtrString=new char[len+1];
    zCopy(source,zPtrString);
    zLength=len;
}

zUlong ZString::zStrLen(const char* source) const
{
    zUlong counter=0;
    char* stemp=(char*)source;

    while(*stemp++)counter++;
    return counter;
}

const char* ZString::to_Cstr() const
{
    return zPtrString;
}

zUlong ZString::zSize() const
{
    return zLength;
}


bool ZString::zIsEmpty() const
{
    return zPtrString==nullptr || zLength==0;
}

void ZString::zClear()
{
    zCopyAlloc("");
}

bool ZString::zCompare(const char* str) const
{
    if(zSize() != zStrLen(str)) return false;

    char* stemp1=(char*)str;
    char* stemp2=(char*)zPtrString;

    while(*stemp1 && *stemp2)
    {
        if(*stemp1!=*stemp2) return false;
        stemp1++;
        stemp2++;
    }
    return true;
}


bool ZString::zCompare(const ZString& str) const
{
    if(zSize() != str.zSize()) return false;

    char* stemp1=(char*)str.to_Cstr();
    char* stemp2=(char*)zPtrString;

    while(*stemp1 && *stemp2)
    {
        if(*stemp1!=*stemp2) return false;
        stemp1++;
        stemp2++;
    }
    return true;
}



 ZString::~ZString()
 {
     if(zPtrString!=nullptr)
     {
         delete[] zPtrString;
     }
 }
