
#ifndef ZSTRING_H
#define ZSTRING_H

typedef unsigned long zUlong;

class ZString
{
private:
    char* zPtrString;
    zUlong zLength;


    void zCopy(const char* source,char* destination);
    void zCopyAlloc(const char* source);
    zUlong zStrLen(const char* source) const;
public:

    ZString();
    ZString(const char* source);
    ZString(const ZString& source);

  
    ZString& operator=(const ZString& source)
    {
        if(this!=&source)
        {
            zCopyAlloc(source.to_Cstr());
        }
        return *this;
    }

    ZString& operator=(const char* source)
    {
        if(source!=nullptr)
        {
            zCopyAlloc(source);
        }
        return *this;
    }

    char& operator[] (zUlong pos)
    {
        return zPtrString[pos];
    }

    const char& operator[] (zUlong pos) const
    {
        return zPtrString[pos];
    }

    bool operator==(const ZString& str)const
    {
        return this->zCompare(str);
    }


    const char* to_Cstr() const;
    zUlong zSize() const;
    bool zIsEmpty() const;
    void zClear();
    bool zCompare(const char *str) const;
    bool zCompare(const ZString& str) const;

   
    ~ZString();
};
#endif // ZSTRING_H