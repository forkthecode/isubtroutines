
function getOperatingSystem: string;
var
osVersionInfo:TOSVersionInfo;
majorVersion,minorVersion:dword;
begin
   Result:='Unknown';
   osVersionInfo.dwOSVersionInfoSize:=SizeOf(TOSVersionInfo);

   if GetVersionEx(osVersionInfo) then
   begin

     majorVersion:=osVersionInfo.dwMajorVersion;
     minorVersion:=osVersionInfo.dwMinorVersion;

     if (majorVersion=10) and (minorVersion=0) then
       Result:='Windows 10'

     else if (majorVersion=6) and (minorVersion=3) then
       Result:='Windows 8.1'

     else if (majorVersion=6) and (minorVersion=2) then
        Result:='Windows 8'

     else if (majorVersion=6) and (minorVersion=1) then
        Result:='Windows 7'

     else if (majorVersion=6) and (minorVersion=0) then
        Result:='Windows vista'

     else if (majorVersion=5) and (minorVersion=1) then
        Result:='Windows xp'

   end;
end;
