
uses
  SysUtils,ActiveX,URLMon;

type
 TBuffer=Array of Byte;

Function downloadFileBuffer(const URL:String):TBuffer;
var
stream:IStream;
sizeFile,sizeSet,bytesWritten:Int64;
buffer:TBuffer;
begin
 Result:=nil;
 if URLOpenBlockingStream (nil,pchar(URL),stream,0,nil)=S_OK then
 begin
     stream.Seek(0,STREAM_SEEK_END,sizeFile);
     SetLength(buffer,sizeFile);
     stream.Seek(0,STREAM_SEEK_SET,sizeSet);
     stream.Read(@buffer[0],sizeFile,@bytesWritten);
     Result:=buffer;
 end;
end;

var
url:String;
buffer:TBuffer;
begin
  url:='http://i67.tinypic.com/2v8lv88.png';
  buffer:=downloadFileBuffer(url);

  if buffer<>nil then
     Writeln('Tamano del fichero leido ',Length(buffer))
  else
     Writeln('Hubo un error ');

  Readln;
end.