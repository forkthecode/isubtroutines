uses
  SysUtils,windows,shellApi;



function ShellExecuteWait(hWnd:HWND;Operation,Filename,Parameters,Directory:PAnsiChar;ShowCmd:Integer):Cardinal;
var
  ExecuteInfo:TShellExecuteInfoA;
  IsShellOk:LongBool;
begin
  ExecuteInfo.cbSize:=sizeof(TShellExecuteInfoA);
  ExecuteInfo.fMask:=SEE_MASK_NOCLOSEPROCESS;
  ExecuteInfo.Wnd:=hWnd;
  ExecuteInfo.lpVerb:=Operation;
  ExecuteInfo.lpFile:=Filename;
  ExecuteInfo.lpParameters:=Parameters;
  ExecuteInfo.nShow:=ShowCmd;

  IsShellOk:=ShellExecuteEx(@ExecuteInfo);
  if not IsShellOk then
  begin
    Result:=GetLastError;
    Exit;
  end
  else Result:=ExecuteInfo.hInstApp;

  if ExecuteInfo.hProcess<>0 then
    Result:=WaitForSingleObject(ExecuteInfo.hProcess,INFINITE);
end;

begin
  ShellExecuteWait(0,'open','calc.exe',nil,nil,SW_SHOWNORMAL);
  ShellExecuteWait(0,'open','notepad.exe',nil,nil,SW_SHOWNORMAL);
end.
